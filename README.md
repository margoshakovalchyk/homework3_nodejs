# NODE.js HOMEOWRK 3
## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and 
testing purposes.
## Requirements

Make sure you have:
* [nodejs](https://nodejs.org) >= 12.11.1
* [npm](https://docs.npmjs.com/) >= 6.11.3 or [yarn](https://yarnpkg.com) >= 1.19.1
* Linux/OS X or Windows [git bash](https://git-scm.com/downloads)

Clone git repo.
```bash
$ git clone https://gitlab.com/margoshakovalchyk/homework3_nodejs
```

Go to app root dir.
```bash
$ cd homework3_nodejs
```

Install dependencies.
```bash
$ yarn install
```
or
```bash
$ npm install
```
## Running the app

Run the app.
```bash
$ npm start
```
## Core maintainer

* [**Margarita Kovalchuk**](https://gitlab.com/margoshakovalchyk)