require('dotenv').config();
const fs = require('fs');
const path = require('path');
const port = process.env.PORT;

const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadRouter = require('./routers/loadRouter');
const {fail} = require('./helpers/errorHandler');

const accessLogStream = fs.createWriteStream(
    path.join(__dirname, 'logs.txt'),
    {flags: 'a'});

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('METHOD: :method  URL: :url  STATUS :status  DATE: [:date[clf]]',
    {stream: accessLogStream}));

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadRouter);

app.use((err, req, res, next) => {
  fail(err, res);
});

const start = async () => {
  await mongoose.connect('mongodb+srv://marharyta_kovalchuk:KsKuvUhaizy9cuz@cluster0.xtsey.mongodb.net/marharyta_kovalchuk?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();
