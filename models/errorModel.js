/* eslint-disable require-jsdoc */
class UnauthorizedError extends Error {
  constructor(message = 'Unauthorized Error') {
    super(message);
    this.statusCode = 401;
  }
}

class UserExistanceError extends Error {
  constructor(message = 'User does`t exist') {
    super(message);
    this.statusCode = 400;
  }
}

class PasswordMatchError extends Error {
  constructor(message = 'Passwords don`t match') {
    super(message);
    this.statusCode = 400;
  }
}

class PermitionDeniedError extends Error {
  constructor(message = 'You don`t have rights to do that') {
    super(message);
    this.statusCode = 400;
  }
}

class BadRequestError extends Error {
  constructor(message = 'Not found') {
    super(message);
    this.statusCode = 400;
  }
}

class HeaderError extends Error {
  constructor(message = 'No Authorization http header found!') {
    super(message);
    this.statusCode = 400;
  }
}

class JWTTokenError extends Error {
  constructor(message = 'No JWT token found!') {
    super(message);
    this.statusCode = 400;
  }
}

module.exports = {
  UnauthorizedError,
  UserExistanceError,
  BadRequestError,
  JWTTokenError,
  HeaderError,
  PasswordMatchError,
  PermitionDeniedError,
};
