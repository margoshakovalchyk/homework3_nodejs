/* eslint-disable new-cap */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const loadSchema = Schema({
  created_by: {
    type: String,
    required: true},
  assigned_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    default: 'NEW',
    required: true,
  },
  state: {
    type: String,
    default: null,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    required: true,
  },
  logs: {type: Array,
    default: [],
  },
});

module.exports.Load = mongoose.model('Load', loadSchema);
