/* eslint-disable require-jsdoc */
const mongoose = require('mongoose');
const {Truck} = require('../truckModel');
const {BadRequestError} = require('../../models/errorModel');

class TruckDao {
  async findBy(options) {
    const truck = await this.findAll(options);
    this.checkExistance(truck);
    return truck;
  }

  async findAll({...options}) {
    return await Truck.find(options, {__v: 0});
  }

  async findFirst(options) {
    return await Truck.findOne(options);
  }

  checkExistance(truck) {
    if (!truck) {
      throw new BadRequestError('Truck not found');
    }
  }

  async save(id, type) {
    const truck = new Truck({
      // eslint-disable-next-line new-cap
      created_by: mongoose.Types.ObjectId(id),
      type,
    });
    await truck.save();
  }

  async delete(options) {
    await Truck.deleteOne(options);
  }

  async deleteAll(options) {
    await Truck.deleteMany(options);
  }

  async updateOneField(filter, options) {
    await Truck.updateOne(filter, options);
  }
}

module.exports = TruckDao;
