/* eslint-disable require-jsdoc */
const {Load} = require('../loadModel');
const {BadRequestError} = require('../../models/errorModel');

class LaodDao {
  async findBy(options) {
    const load = await this.findAll(options);
    this.checkExistance(load);
    return load;
  }

  async findAll({...options}) {
    return await Load.find(options, {__v: 0});
  }

  async findWithPagination(options) {
    return await Load.find(...options);
  }

  async findFirst(options) {
    return await Load.findOne(options, {__v: 0});
  }

  checkExistance(load) {
    if (!load ) {
      throw new BadRequestError('Load not found');
    }
  }

  async save({pickupAddress, deliveryAddress, ...otherOptions}) {
    const load = new Load({
      ...otherOptions,
      pickup_address: pickupAddress,
      delivery_address: deliveryAddress,
    });
    await load.save();
  }

  async delete(options) {
    await Load.deleteOne(options);
  }

  async deleteAll(options) {
    await Load.deleteMany(options);
  }

  async updateOneField(filter, options) {
    await Load.updateOne(filter, options);
  }
}

module.exports = LaodDao;
