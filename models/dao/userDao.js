/* eslint-disable require-jsdoc */
const {UserExistanceError} = require('../../models/errorModel');
const {User} = require('../../models/userModel');

class UserDao {
  async findByRegister(options) {
    const user = await User.findOne(options);
    this.checkRegistration(user);
    return user;
  }

  async findBy(options) {
    const user = await User.findOne(options, {__v: 0});
    this.checkExistance(user);
    return user;
  }

  checkRegistration(user) {
    if (user) {
      throw new UserExistanceError(`User with this email already created`);
    }
  }

  checkExistance(user) {
    if (!user) {
      throw new UserExistanceError();
    }
  }

  async save(options) {
    const user = new User(options);
    await user.save();
  }

  async delete(options) {
    await User.deleteOne(options);
  }
}

module.exports = UserDao;
