/* eslint-disable require-jsdoc */
const bcrypt = require('bcrypt');
const {Credential} = require('../credentialModel');
const {UserExistanceError} = require('../../models/errorModel');

class CredentialDao {
  async findBy(options) {
    const user = await Credential.findOne(options, {__v: 0});
    this.checkExistance(user);
    return user;
  }

  checkExistance(user) {
    if (!user) {
      throw new UserExistanceError();
    }
  }

  async save({password, email, role}) {
    const credential = new Credential({
      email,
      password: await bcrypt.hash(password, 10),
      role,
    });
    await credential.save();
  }

  async delete(options) {
    await Credential.deleteOne(options);
  }

  async updateOneField(filter, options) {
    await Credential.updateOne(filter, options);
  }
}

module.exports = CredentialDao;
