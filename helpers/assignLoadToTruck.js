const LoadDao = require('../models/dao/loadDao');
const {logger} = require('./logger');

module.exports.assignLoadToTruck = async (trucks, truckType, load) => {
  const loadInstance = new LoadDao();

  const assignTruck = trucks.find((truck, i, arr) => {
    if (truck.type !== truckType.type) {
      return arr[arr.length-1];
    } else {
      return truck;
    }
  });

  await loadInstance.updateOneField(
      {_id: load._id},
      {
        assigned_to: assignTruck.assigned_to,
      });

  await logger(load._id,
      `Load assigned to driver with id ${assignTruck.assigned_to}`);

  return assignTruck;
};

