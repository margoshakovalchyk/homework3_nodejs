const TruckDao = require('../models/dao/truckDao');
const UserDao = require('../models/dao/userDao');


module.exports.checkTruckAccess = async (id, email) => {
  const truckInstance = new TruckDao();
  const userInstance = new UserDao();

  const {_id: userId} = await userInstance.findBy({email});
  const truck = await truckInstance.findFirst({_id: id});

  await truckInstance.checkExistance(truck);

  if (userId != truck.created_by) {
    throw new PermitionDeniedError();
  }

  return {truck, userId};
};

