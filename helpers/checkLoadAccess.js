const LoadDao = require('../models/dao/loadDao');
const UserDao = require('../models/dao/userDao');
const {PermitionDeniedError} = require('../models/errorModel');

module.exports.checkLoadAccess = async (id, email) => {
  const loadInstance = new LoadDao();
  const userInstance = new UserDao();

  const {_id: userId} = await userInstance.findBy({email});
  const load = await loadInstance.findFirst({_id: id});

  await loadInstance.checkExistance(load);

  if (userId != load.created_by) {
    throw new PermitionDeniedError();
  }

  return {load, userId};
};

