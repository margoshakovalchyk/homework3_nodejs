const LoadDao = require('../models/dao/loadDao');

module.exports.logger = async (id, message) => {
  const load = new LoadDao();

  const log = {
    message,
    time: Date.now(),
  };

  await load.updateOneField(
      {_id: id},
      {$push: {logs: log}});
};
