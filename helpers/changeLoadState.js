const LoadDao = require('../models/dao/loadDao');
const {logger} = require('../helpers/logger');
module.exports.changeLoadState = async (loadId) => {
  const status = 'ASSIGNED';
  const state = 'En Route to Pick Up';
  const loadInstance = new LoadDao();

  await loadInstance.updateOneField(
      {_id: loadId},
      {status, state},
  );

  await logger(loadId, `Change status to ${status}`);
  await logger(loadId, `Change state to ${state}`);
};
