const TruckDao = require('../models/dao/truckDao');
const {TRUCK_TYPES} = require('../constants');
const {logger} = require('./logger');
const {assignLoadToTruck} = require('./assignLoadToTruck');
const {changeTruckStatus} = require('../helpers/changeTruckStatus');
const {changeLoadState} = require('../helpers/changeLoadState');

module.exports.isDriverFound = async (load) => {
  let isFound = true;
  const truckInstance = new TruckDao();
  const {
    width: LoadWidth,
    length: LoadLength,
    height: LoadHeight,
  } = load.dimensions;
  const {payload: LoadPayload, _id} = load;

  const LoadDimensions = LoadWidth*LoadLength*LoadHeight;

  const trucks = await truckInstance.findBy({
    assigned_to: {$ne: null},
    status: 'IS',
  });

  const truckType = TRUCK_TYPES.find((truck) => {
    const {width, length, height, payload: TruckPayload} = truck;
    const truckDimensions = width*length*height;

    if (LoadDimensions <= truckDimensions && LoadPayload <= TruckPayload) {
      return truck;
    }
  });

  if (!truckType || !trucks.length) {
    await logger(_id, 'No free driver');
    isFound = false;
    return isFound;
  }

  const assignedTruck = await assignLoadToTruck(trucks, truckType, load);
  await changeTruckStatus(assignedTruck);
  await changeLoadState(_id);

  return isFound;
};
