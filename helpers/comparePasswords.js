const bcrypt = require('bcrypt');
const {PasswordMatchError} = require('../models/errorModel');

module.exports.comparePasswords = async (firstPassword, secondPassword) => {
  if (!await bcrypt.compare(firstPassword, secondPassword)) {
    throw new PasswordMatchError;
  }
};
