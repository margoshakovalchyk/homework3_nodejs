const TruckDao = require('../../models/dao/truckDao');
const UserDao = require('../../models/dao/userDao');

module.exports.addTruck = async (req, res) => {
  let {type} = req.body;
  const {email} = req.user;
  type = type.toUpperCase();

  const user = await new UserDao().findBy({email});
  const truck = new TruckDao();
  await truck.save(user._id, type);

  res.json({message: 'Truck created successfully'});
};
