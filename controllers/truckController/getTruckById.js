const {checkTruckAccess} = require('../../helpers/checkTruckAccess');

module.exports.getTruckById = async (req, res) => {
  const {id} = req.params;
  const {email} = req.user;

  const oneTruck = await checkTruckAccess(id, email);

  res.json({'truck': oneTruck});
};
