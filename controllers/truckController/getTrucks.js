/* eslint-disable new-cap */
const mongoose = require('mongoose');
const TruckDao = require('../../models/dao/truckDao');
const UserDao = require('../../models/dao/userDao');

module.exports.getTrucks = async (req, res) => {
  const {email} = req.user;
  const {offset = 0, limit = 10} = req.query;

  const user = await new UserDao().findBy({email});
  const trucks = await new TruckDao().findAll(
      {created_by: mongoose.Types.ObjectId(user._id)},
      [],
      {
        offset: parseInt(offset),
        limit: limit > 100 ? 10 : parseInt(limit),
      },
  );

  res.json({trucks});
};
