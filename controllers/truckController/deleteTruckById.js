const TruckDao = require('../../models/dao/truckDao');
const {checkTruckAccess} = require('../../helpers/checkTruckAccess');
const {PermitionDeniedError} = require('../../models/errorModel');

module.exports.deleteTruckById = async (req, res) => {
  const {id} = req.params;
  const {email} = req.user;

  const truckInstance = new TruckDao();

  const truck = await truckInstance.findFirst({_id: id});

  if (truck.assigned_to != null || truck.status == 'OL') {
    throw new PermitionDeniedError('You can`t delete truck until you on LOAD');
  }

  await checkTruckAccess(id, email);

  await truckInstance.delete({_id: id});
  res.json({message: 'Truck deleted successfully'});
};
