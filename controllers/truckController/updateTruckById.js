const TruckDao = require('../../models/dao/truckDao');
const {checkTruckAccess} = require('../../helpers/checkTruckAccess');
const {PermitionDeniedError} = require('../../models/errorModel');

module.exports.updateTruckById = async (req, res) => {
  const {id} = req.params;
  const {type} = req.body;
  const {email} = req.user;

  const truckInstance = new TruckDao();
  await checkTruckAccess(id, email);

  const truck = await truckInstance.findFirst({_id: id});

  if (truck.assigned_to != null || truck.status == 'OL') {
    throw new PermitionDeniedError('You can`t change until you are on LOAD');
  }

  await truckInstance.updateOneField({_id: id}, {$set: {'type': type}});

  res.json({message: 'Truck details changed successfully'});
};
