const {checkTruckAccess} = require('../../helpers/checkTruckAccess');
const TruckDao = require('../../models/dao/truckDao');
const {BadRequestError} = require('../../models/errorModel');

module.exports.assignTruckById = async (req, res) => {
  const {id} = req.params;
  const {email} = req.user;

  const truckInstance = new TruckDao();

  const {truck, userId} = await checkTruckAccess(id, email);

  const userTrucks = await truckInstance.findAll({created_by: userId});

  if (userTrucks.some((truck) => truck.assigned_to)) {
    throw new BadRequestError('One is assigned. You can`t assign more');
  }

  await truckInstance.updateOneField(
      {_id: id},
      {assigned_to: truck.created_by},
  );

  res.json({message: 'Truck assigned successfully'});
};

