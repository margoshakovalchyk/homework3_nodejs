require('dotenv').config();
const UserDao = require('../../models/dao/userDao');
const CredentialDao = require('../../models/dao/credentialDao');

module.exports.registration = async (req, res) => {
  let {email, password, role} = req.body;
  role = role.toUpperCase();

  const user = new UserDao();
  const credentials = new CredentialDao();

  await user.findByRegister({email});

  await user.save({email});
  await credentials.save({email, password, role});

  res.json({message: 'Profile created successfully'});
};
