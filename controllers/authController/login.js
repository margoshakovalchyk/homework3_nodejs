require('dotenv').config();
const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET;
const CredentialDao = require('../../models/dao/credentialDao');
const {comparePasswords} = require('../../helpers/comparePasswords');

module.exports.login = async (req, res) => {
  const {email, password} = req.body;

  const user = await new CredentialDao().findBy({email});

  await comparePasswords(password + '', user.password);

  const jwtToken = jwt.sign({
    email: user.email,
    role: user.role,
    __id: user.__id,
  }, JWT_SECRET);

  res.json({'jwt_token': jwtToken});
};
