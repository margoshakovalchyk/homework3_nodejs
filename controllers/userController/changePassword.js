const bcrypt = require('bcrypt');
const CredentialDao = require('../../models/dao/credentialDao');
const {comparePasswords} = require('../../helpers/comparePasswords');

module.exports.changePassword = async (req, res) => {
  const {email} = req.user;
  const {oldPassword, newPassword} = req.body;

  const credential = new CredentialDao();
  const user = await credential.findBy({email});

  await comparePasswords(oldPassword + '', user.password);

  const password = await bcrypt.hash(newPassword, 10);

  await credential.updateOneField({email}, {$set: {password}});

  res.json({'message': 'Password changed successfully'});
};

