const UserDao = require('../../models/dao/userDao');

module.exports.getUserInfo = async (req, res) => {
  const {email} = req.user;

  const user = await new UserDao().findBy({email});

  res.json({user});
};
