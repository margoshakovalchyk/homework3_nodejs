const UserDao = require('../../models/dao/userDao');
const CredentialDao = require('../../models/dao/credentialDao');
const LoadDao = require('../../models/dao/loadDao');
const TruckDao = require('../../models/dao/truckDao');
const {PermitionDeniedError} = require('../../models/errorModel');

module.exports.deleteProfile = async (req, res) => {
  const {email} = req.user;

  const user = new UserDao();
  const credentials = new CredentialDao();
  const truckInstance = new TruckDao();
  const load = new LoadDao();

  const profile = await credentials.findBy({email});
  const truck = truckInstance.findFirst({created_by: profile._id});

  if (truck.status = 'OL') {
    throw new PermitionDeniedError('You can`t delete yourself are on LOAD');
  }

  if (profile.role == 'SHIPPER') {
    await load.deleteAll({created_by: profile._id});
  } else {
    await truckInstance.deleteAll({created_by: profile._id});
  }

  await user.delete({email});
  await credentials.delete({email});

  res.json({'message': 'Profile deleted successfully'});
};
