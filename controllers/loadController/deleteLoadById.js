const LoadDao = require('../../models/dao/loadDao');
const {checkLoadAccess} = require('../../helpers/checkLoadAccess');
const {PermitionDeniedError} = require('../../models/errorModel');


module.exports.deleteLoadById = async (req, res) => {
  const {id} = req.params;
  const {email} = req.user;

  const loadInstance = new LoadDao();

  await checkLoadAccess(id, email);

  const load = await loadInstance.findFirst({_id: id});
  if (load.status !== 'NEW') {
    throw new PermitionDeniedError('You can`t delete load');
  }

  await loadInstance.delete({_id: id});
  res.json({message: 'Load deleted successfully'});
};
