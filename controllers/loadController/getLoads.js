/* eslint-disable new-cap */
const LoadDao = require('../../models/dao/loadDao');
const UserDao = require('../../models/dao/userDao');

module.exports.getLoads = async (req, res) => {
  const query = {};
  const {email, role} = req.user;
  const {offset = 0, limit = 10, status = 'SHIPPED'} = req.query;

  const user = await new UserDao().findBy({email});

  if (role == 'DRIVER' ) {
    query.assigned_to = user._id;
  } else {
    query.created_by = user._id;
  }

  if (status) {
    query.status = status;
  }

  const loads = await new LoadDao().findWithPagination(
      [query,
        {__v: 0},
        {
          offset: parseInt(offset),
          limit: limit > 50 ? 10 : parseInt(limit),
        }],
  );

  res.json({loads});
};
