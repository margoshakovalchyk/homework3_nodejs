const LoadDao = require('../../models/dao/loadDao');
const UserDao = require('../../models/dao/userDao');

module.exports.addLoad = async (req, res) => {
  const {email} = req.user;

  const {
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    ...otherKeys
  } = req.body;

  const user = await new UserDao().findBy({email});
  const load = new LoadDao();

  await load.save({
    ...otherKeys,
    pickupAddress,
    deliveryAddress,
    created_by: user._id,
  });

  res.json({message: 'Load created successfully'});
};
