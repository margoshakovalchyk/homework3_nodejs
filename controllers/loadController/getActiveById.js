const LoadDao = require('../../models/dao/loadDao');

module.exports.getActiveById = async (req, res) => {
  const {id} = req.params;

  const loadInstance = new LoadDao();

  const load = await loadInstance.findBy({_id: id});
  res.json({load});
};
