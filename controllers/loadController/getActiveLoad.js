const LoadDao = require('../../models/dao/loadDao');
const UserDao = require('../../models/dao/userDao');

module.exports.getActiveLoad = async (req, res) => {
  const {email} = req.user;

  const loadInstance = new LoadDao();
  const userInstance = new UserDao();

  const {_id: userId} = await userInstance.findBy({email});
  const load = await loadInstance.findBy({assigned_to: userId});

  res.json({load});
};
