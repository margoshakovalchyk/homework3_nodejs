const {checkLoadAccess} = require('../../helpers/checkLoadAccess');

module.exports.getLoadById = async (req, res) => {
  const {id} = req.params;
  const {email} = req.user;

  const {load} = await checkLoadAccess(id, email);

  res.json({load});
};
