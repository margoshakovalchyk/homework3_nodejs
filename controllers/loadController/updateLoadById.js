const LoadDao = require('../../models/dao/loadDao');
const {PermitionDeniedError} = require('../../models/errorModel');
const {checkLoadAccess} = require('../../helpers/checkLoadAccess');

module.exports.updateLoadById = async (req, res) => {
  const {id} = req.params;
  const {email} = req.user;
  const {
    name,
    payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
  } = req.body;
  const loadInstance = new LoadDao();

  await checkLoadAccess(id, email);

  const load = await loadInstance.findFirst({_id: id});
  if (load.status !== 'NEW') {
    throw new PermitionDeniedError('You can`t change load');
  }

  await loadInstance.updateOneField(
      {_id: id},
      {$set: {name,
        payload,
        pickup_address: pickupAddress,
        delivery_address: deliveryAddress,
        dimensions}});

  res.json({message: 'Load details changed successfully'});
};
