/* eslint-disable require-jsdoc */
const LoadDao = require('../../models/dao/loadDao');
const {isDriverFound} = require('../../helpers/isDriverFound');
const {logger} = require('../../helpers/logger');
const {BadRequestError} = require('../../models/errorModel');


module.exports.postLoadById = async (req, res) => {
  const {id} = req.params;

  const load = new LoadDao();

  const loadFromId = await load.findFirst({_id: id});

  await load.checkExistance(loadFromId);
  if (loadFromId.status == 'POSTED') {
    throw new BadRequestError('Load is already posted');
  }

  await load.updateOneField({_id: id}, {status: 'POSTED'});

  await logger(id, 'Post a load');

  const isFound = await isDriverFound(loadFromId);

  if (!isFound) {
    await load.updateOneField({_id: id}, {status: 'NEW', state: null});
  }

  const message = isFound ? 'Load posted successfully' : 'Driver wasn`t found';

  res.json({
    message,
    driver_found: isFound,
  });
};
