const ROLES = ['SHIPPER', 'DRIVER'];

const LOAD_STATUSES = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];

const LOAD_STATE_TRANSITIONS = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to Delivery',
  'Arived to Delivery',
];

const TRUCK_STATUSES = {
  is: 'in service',
  ol: 'on load',
};

const TRUCK_TYPES = [
  {
    type: 'SPRINTER',
    width: 300,
    length: 250,
    height: 170,
    payload: 1700,
  },
  {
    type: 'SMALL STRAIGHT',
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
  },
  {
    type: 'LARGE STRAIGHT',
    width: 700,
    length: 350,
    height: 200,
    payload: 4000,
  },
];

module.exports = {
  ROLES,
  TRUCK_STATUSES,
  LOAD_STATUSES,
  TRUCK_TYPES,
  LOAD_STATE_TRANSITIONS,
};
