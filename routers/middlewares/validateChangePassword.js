const Joi = require('joi');

module.exports.validateChangePassword = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .required(),

    newPassword: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};
