const Joi = require('joi');

module.exports.validateLogin = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .required(),

    password: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};
