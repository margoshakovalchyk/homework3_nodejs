/* eslint-disable require-jsdoc */
const {PermitionDeniedError} = require('../../models/errorModel');

module.exports.validateShipper = (req, res, next) => {
  let {role} = req.user;
  role = role.toUpperCase();

  if (role != 'SHIPPER') {
    throw new PermitionDeniedError;
  }

  next();
};

