const Joi = require('joi');

module.exports.validateLoad = async (req, res, next) => {
  const schema = Joi.object({
    'name': Joi.string().required(),

    'payload': Joi.number().required(),

    'pickup_address': Joi.string().required(),

    'delivery_address': Joi.string().required(),

    'dimensions': Joi.object({
      'width': Joi.number().required(),
      'length': Joi.number().required(),
      'height': Joi.number().required(),
    }),
  });

  await schema.validateAsync(req.body);
  next();
};
