/* eslint-disable require-jsdoc */
const {PermitionDeniedError} = require('../../models/errorModel');

module.exports.validateDriver = (req, res, next) => {
  let {role} = req.user;
  role = role.toUpperCase();

  if (role != 'DRIVER') {
    throw new PermitionDeniedError;
  }

  next();
};

