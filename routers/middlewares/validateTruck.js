const Joi = require('joi');
const {TRUCK_TYPES} = require('../../constants');

module.exports.validateTruck = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
        .valid(...TRUCK_TYPES.map((truck) => truck.type))
        .required()
        .insensitive(),
  });

  await schema.validateAsync(req.body);
  next();
};
