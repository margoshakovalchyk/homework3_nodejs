/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../helpers/helper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateLoad} = require('./middlewares/validateLoad');
const {validateShipper} = require('./middlewares/validateShipper');
const {validateDriver} = require('./middlewares/validateDriver');

const {getLoads} = require('../controllers/loadController/getLoads');
const {iterateLoadState} =
require('../controllers/loadController/iterateLoadState');
const {addLoad} = require('../controllers/loadController/addLoad');
const {postLoadById} = require('../controllers/loadController/postLoadById');
const {getLoadById} = require('../controllers/loadController/getLoadById');
const {deleteLoadById} =
require('../controllers/loadController/deleteLoadById');
const {updateLoadById} =
require('../controllers/loadController/updateLoadById');
const {getActiveLoad} =
require('../controllers/loadController/getActiveLoad');
const {getActiveById} =
require('../controllers/loadController/getActiveById');

router.use(authMiddleware);

router.get('/', asyncWrapper(getLoads));
router.patch('​/active/state', validateDriver, asyncWrapper(iterateLoadState));
router.get('/active/', validateDriver, asyncWrapper(getActiveLoad));

router.post('/',
    validateShipper,
    asyncWrapper(validateLoad),
    asyncWrapper(addLoad));

router.use('/:id', validateShipper);
router.post('/:id/post', asyncWrapper(postLoadById));
router.get('/:id', asyncWrapper(getLoadById));
router.delete('/:id', asyncWrapper(deleteLoadById));
router.put('/:id', asyncWrapper(validateLoad), asyncWrapper(updateLoadById));
router.get('/:id/shipping_info', asyncWrapper(getActiveById));

module.exports = router;
