/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../helpers/helper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateTruck} = require('./middlewares/validateTruck');
const {validateDriver} = require('./middlewares/validateDriver');

const {getTrucks} = require('../controllers/truckController/getTrucks');
const {addTruck} = require('../controllers/truckController/addTruck');
const {getTruckById} = require('../controllers/truckController/getTruckById');
const {deleteTruckById} =
require('../controllers/truckController/deleteTruckById');
const {updateTruckById} =
require('../controllers/truckController/updateTruckById');
const {assignTruckById} =
require('../controllers/truckController/assignTruckById');

router.use(authMiddleware);
router.use(validateDriver);
router.get('/', asyncWrapper(getTrucks));
router.post('/', asyncWrapper(validateTruck), asyncWrapper(addTruck));

router.put('/:id', asyncWrapper(validateTruck), asyncWrapper(updateTruckById));
router.get('/:id', asyncWrapper(getTruckById));
router.post('/:id/assign', asyncWrapper(assignTruckById));
router.delete('/:id', asyncWrapper(deleteTruckById));

module.exports = router;
