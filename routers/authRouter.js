/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../helpers/helper');
const {validateRegistration} = require('./middlewares/validateRegistration');
const {validateLogin} = require('./middlewares/validateLogin');

const {login} = require('../controllers/authController/login');
const {registration} = require('../controllers/authController/registration');

router.post('/register', asyncWrapper(validateRegistration),
    asyncWrapper(registration));
router.post('/login', asyncWrapper(validateLogin), asyncWrapper(login));

module.exports = router;
