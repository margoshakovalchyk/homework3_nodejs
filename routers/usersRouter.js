/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../helpers/helper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateChangePassword} =
require('./middlewares/validateChangePassword');

const {getUserInfo} = require('../controllers/userController/getUserInfo');
const {deleteProfile} = require('../controllers/userController/deleteProfile');
const {changePassword} =
require('../controllers/userController/changePassword');

router.use(authMiddleware);
router.get('/me', asyncWrapper(getUserInfo));
router.patch('/me/password', asyncWrapper(validateChangePassword),
    asyncWrapper(changePassword));
router.delete('/me', asyncWrapper(deleteProfile));

module.exports = router;
